# z0s/http

### Installation
You can include it in your project using: `composer require z0s/http`

### Requirements
1. PSR-11 compatible container
2. PHP8.0 or higher
