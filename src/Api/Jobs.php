<?php

namespace z0s\RoadRunner\Api;

use LogicException;
use Spiral\RoadRunner\Jobs\Jobs as RoadRunnerJobs;

abstract class Jobs
{
    protected bool $retry = false;
    protected int $retryAttempts = 0;
    protected int $retryDelay = 0;
    protected string $queue = 'default';

    public function enqueue(array $payload): void
    {
        $jobsWorker = new RoadRunnerJobs();
        if (!$jobsWorker->isAvailable()) {
            throw new LogicException('The server does not support the jobs feature.');
        }

        $queue = $jobsWorker->connect($this->queue);
        $task = $queue->create(get_class($this), $payload)
            ->withHeader('requeue', (bool) $this->retry)
            ->withHeader('attempts', (int) $this->retryAttempts)
            ->withHeader('retryDelay', (int) $this->retryDelay);

        $queue->dispatch($task);
    }

    abstract public function handle(array $payload): void;
}
